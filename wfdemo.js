(function($) {
  Drupal.behaviors.wfdemo_multiselect = {
    attach: function(context, settings) {
      $('select.multiselect', context).once('wfdemo-multiselect').multiselect({});
    }
  }
})(jQuery);